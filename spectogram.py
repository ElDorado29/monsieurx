#!/usr/bin/env python3 
#coding:utf-8

import os
from pathlib import Path
from subprocess import run


# Fonction create spectro
def create_spectogram(input_file_path, output_folder_path):

	file_name = input_file_path.with_suffix(".jpg").name
	output_file_path = output_folder_path / file_name

	run(["ffmpeg", "-i", str(input_file_path),"-lavfi", "showspectrumpic=s=1024x512:legend=disabled",str(output_file_path)])
	return output_file_path


# Test
	
if __name__ == "__main__":

	a = create_spectogram(Path("./Dataset/Sons/Voix/11_septembre_2001_13__le_jihad_afghanPart28.mp3"),Path("./Dataset/Images"))
	print(a)
