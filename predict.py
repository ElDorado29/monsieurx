#!/usr/bin/env python3 
#coding:utf-8

import sys

from google.cloud import automl_v1beta1
from google.cloud.automl_v1beta1.proto import service_pb2
from google.protobuf.json_format import MessageToDict

# Fonction pour récupérer le résultat du modèle AutoML vision sur une image
# 'content' is base-64-encoded image data.
def get_prediction(content, project_id, model_id):
  prediction_client = automl_v1beta1.PredictionServiceClient()

  name = 'projects/{}/locations/us-central1/models/{}'.format(project_id, model_id)
  payload = {'image': {'image_bytes': content }}
  params = {}
  request = prediction_client.predict(name, payload, params)
  payload = MessageToDict(request)["payload"][0]
  return payload["displayName"], payload["classification"]["score"] #we return directly image label to make distinction between "Voix" and "Musique" and score to improve model


# Test
if __name__ == '__main__':
  file_path = "./Dataset/Images/Musique/11_septembre_2001_13__le_jihad_afghanPart9.mp3.jpg"

  project_id = "monsieurx"
  model_id = "ICN7293177175270752256"

  with open(file_path, 'rb') as ff:
    content = ff.read()
  print(get_prediction(content, project_id, model_id))
