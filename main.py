#!/usr/bin/env python3 
#coding:utf-8

# Import
import os 										              #pour naviguer
import mutagen									            #pour longueur fichier son
from pydub import AudioSegment				    	#pour split fichier son
from fnmatch import fnmatch					      	#pour filtrer par type de fichier
from mutagen.mp3 import MP3                 #pour gérer le format mp3
from pathlib import Path                    #pour gérer en tant que Path les chemins

from splitaudio import split_audio_file         #fonction split du fichier d'origine en petits morceaux
from spectogram import create_spectogram        #fonction création d'un spectrogramme à partir d'un fichier son
from predict import get_prediction              #fonction pour récupérer les données à partir du modèle entrainé sur Google Vision Automl
from combinesounds import combine_audio_files   #fonction pour recombiner des segments audio en un seul fichier
from combinesounds import sorted_aphanumeric    #fonction pratique pour ordonner les listes

'''
Variables
'''
#Fichiers audio d'origine
dossier_sons_bruts = Path('./EpisodesBruts/')
motif = "*.mp3"

#Split en segments des fichiers audio
duree_segment_ms=1000
dossier_sons_split = Path("./Test/Split")

#Générations des images spectogrammes correspondantes à chacun des segments
output_folder_path = Path("./Test/Images")

#Données Google Vision Automl
project_id = "monsieurx"
model_id = "ICN7293177175270752256"

#Dossier output final
dossier_sons_filtres = Path("./EpisodesFiltres/")

'''
Repérer les fichiers mp3 et lancer la boucle des traitements
'''

# 0 - Lister tous les fichiers mp3 du répertoire d'origine et initier la boucle
episodes = []
for fichier in os.listdir(dossier_sons_bruts):
	if fnmatch(fichier, motif):
		episodes.append(fichier)


# Initier la boucle sur chaque épisode du dossier des émissions brutes
for episode in episodes:
  print(episode)

  # 1 - Découper les fichiers
  split_audio_file(episode,duree_segment_ms,dossier_sons_bruts,dossier_sons_split)

  # 2 - Générer les spectrogrammes
  for fichier_split_mp3 in sorted_aphanumeric(os.listdir(dossier_sons_split)):
    input_file_path = dossier_sons_split / Path(fichier_split_mp3)

    create_spectogram(input_file_path, output_folder_path)

  # 3 - Obtenir la prédiction par spectrogramme depuis le modèle et ajouter les fichiers sons à une liste si la prédiction de leur image renvoie "Voix"
  sounds_list = []

  for fichier in sorted_aphanumeric(os.listdir(output_folder_path)):
    file_path = output_folder_path / Path(fichier)

    with open(file_path, 'rb') as ff:
      content = ff.read()

    category, score = get_prediction(content, project_id, model_id)
    if category == "Voix":
      sounds_list.append(file_path.with_suffix(".mp3").name)

    #   print(file_path, "A GARDER", score)
    # else:
    #   print(file_path, "A SUPPRIMER", score)

  # 4 - Recombiner l'épisode avec les sons listés "Voix"

  sounds_origin_folder = dossier_sons_split
  output_final_folder = dossier_sons_filtres 

  combine_audio_files (sounds_list, sounds_origin_folder, output_final_folder)

  # 5 - Détruire les fichiers splits audio et images spectogrammes intermédiaires

  for f in os.listdir(dossier_sons_split) :
    os.remove(dossier_sons_split / f)

  for f in os.listdir(output_folder_path) :
    os.remove(output_folder_path / f)

