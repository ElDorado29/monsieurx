#!/usr/bin/env python3 
#coding:utf-8

import os
from pydub import AudioSegment					#pour agréger fichier son
from pathlib import Path
import re

#Fonction trouvée sur le net pour ordonner les listes (de manière à avoir Part1, Part2, Part3, etc)
def sorted_aphanumeric(data):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(data, key=alphanum_key)

#Fonction pour agréger les fichiers
def combine_audio_files (sounds_list, sounds_origin_folder, output_final_folder):
    
    sounds_list_paths = [os.path.join(sounds_origin_folder, file) for file in sounds_list]

    combined = AudioSegment.empty()
    for sound in sounds_list_paths:
        combined += AudioSegment.from_mp3(sound)

    without_Part_suffix, _ = str.split(sounds_list[0], 'Part')
    output_file_name = without_Part_suffix + ".mp3"
    output_file_path = output_final_folder / output_file_name

    combined.export(output_file_path, format="mp3")

    return output_file_path


# Test
if __name__ == '__main__':
    sounds_origin_folder = Path('./Test/Split/')
    sounds_list = sorted_aphanumeric(os.listdir('./Test/Split/'))
    sounds_list_paths = [os.path.join('./Test/Split', file) for file in sounds_list]
     
    output_final_folder = Path("./Test/")
    without_Part_suffix, _ = str.split(sounds_list[0], 'Part')
    output_file_name = without_Part_suffix + ".mp3"
    output_file = output_final_folder / output_file_name

    combine_audio_files (sounds_list,sounds_origin_folder,output_final_folder)


